//module "vpc" {
//  source = "terraform-aws-modules/vpc/aws"
//
//  name = "w-test-vpc"
//  cidr = "10.0.0.0/16"
//
//  azs             = ["ap-southeast-1a"]
//  private_subnets = ["10.0.1.0/24"]
//  public_subnets  = ["10.0.101.0/24"]
//
//  enable_nat_gateway = true
//  enable_vpn_gateway = true
//
//  tags = {
//    Terraform = "true"
//    Environment = "dev"
//  }
//}

resource "aws_vpc" "main_vpc" {
  cidr_block = "172.25.0.0/16"

  tags = {
    Name = "w-test-vpc"
  }
}

resource "aws_subnet" "public_subnet" {
  vpc_id     = aws_vpc.main_vpc.id
  cidr_block = "172.25.1.0/24"

  depends_on = [aws_internet_gateway.gw]

  tags = {
    Name = "w-test-vpc-public-subnet"
  }
}

resource "aws_subnet" "private_subnet" {
  vpc_id     = aws_vpc.main_vpc.id
  cidr_block = "172.25.2.0/24"

  depends_on = [aws_internet_gateway.gw]

  tags = {
    Name = "w-test-vpc-private-subnet-a"
  }
}

resource "aws_subnet" "private_subnet_b" {
  vpc_id     = aws_vpc.main_vpc.id
  cidr_block = "172.25.3.0/24"

  depends_on = [aws_internet_gateway.gw]

  tags = {
    Name = "w-test-vpc-private-subnet-b"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main_vpc.id

  tags = {
    Name = "w-test-vpc-igw"
  }
}

//resource "aws_instance" "w_instance" {
//  ami           = "ami-0797ea64"
//  instance_type = "t2.micro"
//  depends_on = ["aws_internet_gateway.gw"]
//}

resource "aws_eip" "w_eip" {
  vpc = true

  //  instance   = "${aws_instance.w_instance.id}"
  depends_on = [aws_internet_gateway.gw]
}

resource "aws_nat_gateway" "gw" {
  allocation_id = aws_eip.w_eip.id
  subnet_id     = aws_subnet.public_subnet.id
  depends_on    = [aws_internet_gateway.gw]

  tags = {
    Name = "w-test-vpc NAT"
  }
}

resource "aws_route_table" "public_r" {
  vpc_id = aws_vpc.main_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "w-test-r-table"
  }
}

resource "aws_route_table" "private_r" {
  vpc_id = aws_vpc.main_vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.gw.id
  }

  tags = {
    Name = "w-test-r-table-private"
  }
}

resource "aws_route_table_association" "arta1" {
  route_table_id = aws_route_table.public_r.id
  subnet_id      = aws_subnet.public_subnet.id
}

resource "aws_route_table_association" "arta2" {
  route_table_id = aws_route_table.private_r.id
  subnet_id      = aws_subnet.private_subnet.id
}

resource "aws_route_table_association" "arta3" {
  route_table_id = aws_route_table.private_r.id
  subnet_id      = aws_subnet.private_subnet_b.id
}