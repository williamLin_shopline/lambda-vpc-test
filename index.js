const axios = require('axios');

exports.handler = async (event) => {
    try {
        var res = await testOutsideConnection();

        const response = {
            statusCode: res.status,
            body: JSON.stringify('Hello from Lambda!'),
        };
        return response;
    } catch(error) {
        const response = {
            statusCode: 500,
            body: JSON.stringify('something wrong'),
        };
        return response;
    }
};


async function testOutsideConnection() {
    try {
        const response = await axios.get('https://www.google.com.tw');
        console.log(response.status);
        return response;
    } catch (error) {
        console.error(response.status);
        return response;
    }
}